function createCard(name, description, pictureUrl,start,end,location) {
    return `
            <div class="card shadow mb-5 bg-white rounded" style="margin-bottom: 20px;">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body" >
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    ${start} - ${end}
                </div>
            </div>
        `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if(!response.ok) {
            throw new Error('I am having trouble finding your conference. Try again.');
        } else {
            const data = await response.json();
            // for styling purposes- 3 items per row
            let currentColumn = 0;


            //const conference = data.conferences[0];
            //const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;


            for (let conference of data.conferences) {
                const detailURL = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details);
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const start = new Date(details.conference.starts).toDateString();
                    const end = new Date(details.conference.ends).toDateString();
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl,start,end,location);
                    const column = document.querySelector(`#col-${currentColumn}`);
                    column.innerHTML += html;
                    currentColumn += 1;
                    if (currentColumn > 2) {
                        currentColumn = 0;
                    }
                }
            }
        }
    } catch (e) {
        // figure out what to do if an error is raised
    }
});
